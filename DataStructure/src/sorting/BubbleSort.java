/**
* The following program performs Bubble sort operation.
* @author  Pavan Patil
* @version 1.0
* @since   6-july-2017 
*/

package sorting;

import java.util.Random;
import java.util.Scanner;

public class BubbleSort {

	public static void main(String[] args) {
		BubbleSort bs=new BubbleSort();
       try
       {
    	   
		long startTime =System.currentTimeMillis();
		bs.sort();
		long endTime=System.currentTimeMillis();
		
		long difference=endTime-startTime;
		System.out.println("Time required is="+difference);
       }
       catch(Exception e )
       {
    	   System.out.println(" Integers only");
       }
		
	}
	
	
	public void sort()
	{
		System.out.println("Enter the size of array");
		Scanner sc=new Scanner(System.in);

		int n=sc.nextInt();
		System.out.println("�nter the elements");
		
		//initialize array 
		int[] a=new int[n];
		
	  // Random rd=new Random();
	  //int rad=rd.nextInt(50);
		
		
		for (int i=0;i<n;i++)
		{
		a[i]=sc.nextInt();
		}
		
		
		//Printing array
//		for(int s:a)
//		{
//			System.out.print("Displying array "+s);
//		}
		
		for(int i=0; i<a.length; i++)
		{
			for(int j=a.length-1;j>0;j--)
			{
				if(a[j]<a[j-1])
				{
					int temp=a[j];
					a[j]=a[j-1];
					a[j-1]=temp;
				}
			}
			for(int k: a)
			{
				System.out.print(k+" ");
			}
			System.out.println();
			
		}	
		
	}
	}

