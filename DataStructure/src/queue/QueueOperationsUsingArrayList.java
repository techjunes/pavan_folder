
/**
* The following program performs Queue operations using ArrayList.
* The program takes maximum of 5 input values. If extra value inserted, the first value will get
* removed and extra value will be added at the last index value.
* @author  Pavan Patil
* @version 1.0
* @since   7-july-2017 
*/

package queue;
import java.util.ArrayList;
import java.util.Scanner;
public class QueueOperationsUsingArrayList {

	public static void main(String[] args) {

		ArrayList alist=new ArrayList();
		int addvalue = 0;
		Scanner sc=new Scanner(System.in);
		while(true)
		{
		System.out.println("Choose operation");
		
		System.out.println("1.Add\n2.Exit");
		int n=sc.nextInt();
		switch(n)
		{
		case 1:System.out.println("Enter element to add");
		if(alist.size()<5)
		{
			 addvalue=sc.nextInt();
			alist.add(addvalue);
			System.out.println(alist);
		}
		else
		{
			alist.remove(0);
			//System.out.println("Enter element to add");
			addvalue=sc.nextInt();
			alist.add(addvalue);
			System.out.println(alist);
			
		}
		break;
		
		case 2:
			System.out.println("You have choose to quit.");
			System.exit(0);
			
		}
		}
	}

}
