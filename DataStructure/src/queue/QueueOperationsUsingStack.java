/**
* The following program performs Queue operations using Stack.
* The program takes maximum of 5 input values. If extra value inserted, the first value will get
* removed and extra value will be added at the last index value.
* @author  Pavan Patil
* @version 1.0
* @since   7-july-2017 
*/
package queue;

import java.util.Scanner;
import java.util.Stack;

//this class will implement queue using stackpublic class QueueUsingStack
public class QueueOperationsUsingStack {
	public static void main(String[] args) {
		Stack inputstack = new Stack();
		Scanner scanner = new Scanner(System.in);
		int choice = 0;
		
          while(true)
          {
			System.out.println("Choose operation:");
			System.out.println("1. Enqueue \n2. Quit");
			
			choice = scanner.nextInt();
			switch (choice) {
			case 1:
				if (inputstack.size() < 5)// check if stack size is less than
											// or equal to 5
				{
					System.out.print("Enter Value to Enqueue: ");
					int addvalue = scanner.nextInt();
					inputstack.push(addvalue);// pushing value in stack
					System.out.println("Queue: " + inputstack);
				} 
				else// when stack size is greater than 5
				{
					inputstack.remove(0);
					System.out.print("Enter Value to Enqueue: ");
					int addvalue = scanner.nextInt();
					inputstack.push(addvalue);// pushing value in stack
					System.out.println("Queue: " + inputstack);
				}
				break;

			case 2:
				System.out.println("You have choose to quit");
               System.exit(0);
			}
	}
	}
}
