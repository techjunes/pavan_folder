/**
* Following program illustrates inserting values into stack and display it in the form of map(key,value).
* @author  Pavan Patil
* @version 1.0
* @since   6-july-2017 
*/

package map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
public class StacktoMap {

	public static void main(String[] args) {
		Stack<String> stack = new Stack<String>();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number of elements");
		int n = sc.nextInt();
		System.out.println("Enter the string value ");
		for (int i = 0; i < n; i++) {
			String k = sc.next();
			stack.push(k);
		}

		// Storing stack value into array
		Object o[] = stack.toArray();

		HashMap<Integer, String> map = new HashMap<Integer, String>();

		// putting array values into map
		for (int i = 0; i < o.length; i++) {
			map.put(i + 1, (String) o[i]);
		}

		// Iterating map and printing.
		// for (Map.Entry<Integer, String> entry : map.entrySet()) {
		// System.out.println("Key = " + entry.getKey() + ", Value = " +
		// entry.getValue());
		// }

		Iterator<Map.Entry<Integer, String>> entry1 = map.entrySet().iterator();
		while (entry1.hasNext()) {

			Map.Entry<Integer, String> e = entry1.next();
			System.out.println("Key = " + e.getKey() + ", Value = " + e.getValue());
		}

	}
}
