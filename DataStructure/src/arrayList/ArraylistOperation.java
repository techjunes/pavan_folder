
/**
* The following program illustrate the operations of ArrayList.
* @author  Pavan Patil
* @version 1.0
* @since   6-july-2017 
*/

package arrayList;

import java.util.ArrayList;
import java.util.Scanner;

public class ArraylistOperation {
	static ArrayList<String> list=new ArrayList<String>();
	
	public static void main(String[] args) {
		ArraylistOperation a= new ArraylistOperation();
	
		Scanner sc=new Scanner(System.in);
		while(true)
		{
		System.out.println("Choose operation");
		
		System.out.println("1.Push\n2.Pop\n3.Remove\n4.Exit");
		int n=sc.nextInt();
		
		switch(n)
		{
		case 1://Push Operation
		System.out.println("Enter element to add into arraylist");
		a.push(sc.next());
		System.out.println("ArrayList:"+list);
		break;
		case 2://Pop operation
			if(!list.isEmpty())
			{
				a.pop(sc.next());
				System.out.println("Last element is removed from arraylist");
				System.out.println("ArrayList"+list);
				
			}
			else
			{
				System.out.println("ArrayList is empty");
			}
			break;
		case 3://Remove all operation
			if(!list.isEmpty())
			{
				list.removeAll(list);
				System.out.println("Removed all elements");
				System.out.println("ArrayList:"+list);
			}
			else
			{
				System.out.println("list is empty");
			}
			break;
		case 4:
			System.out.println("You have choose to quit");
           System.exit(0);
		}
	
		}
	}
	
	private void pop(String next) {
		if(!isEmpty())
		{
		System.out.println("pop Operation");
		list.remove(size()-1);
		}
		else
		{
			System.out.println("ArrayList is empty");
		}
	}
	
	
	private void peek(String next) {
		if(!isEmpty())
		{
		System.out.println("peek Operation");
		list.remove(size()-1);
		}
	}

	private boolean isEmpty() {
		// TODO Auto-generated method stub
		return (list.size()==0);
	}

	public void push(String string)
	{
		System.out.println("push operation");
		list.add(string);
	}
	public int size()
	{
		System.out.println("list.size()");
	return list.size();
	
	}
	}


