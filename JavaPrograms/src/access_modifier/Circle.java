package access_modifier;

public class Circle extends Area{

	static double pi=3.14;
	@Override
	public double circleArea(double radius) {
		
		double circlearea=pi * radius * radius;
		return circlearea;
	}
	@Override
	public double squareArea(double length, double breadth) {
		double squarearea=length * breadth;
		return squarearea;
	}

	

}
