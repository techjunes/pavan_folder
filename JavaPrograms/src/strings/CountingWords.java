package strings;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;


/**
* The following program counts the total number of words presents in a text file irrespective 
* the spaces and special character.
* @author  Pavan Patil
* @version 1.0
* @since   6-july-2017
*/
public class CountingWords {
	final static Logger logger = Logger.getLogger(CountingWords.class.getName());
	
	//main method
	
	public static void main(String[] args) throws IOException {

		// taking file from system
	    logger.info("Taking file from system");
		FileInputStream inputstream = new FileInputStream("C:\\Users\\SRKayCG\\Desktop\\txtfile.txt");

		// reading text from file
		logger.info("Reading text from character-input stream file.");
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputstream));

		String line = "";

		// initilizing counter for counting the words present in file
		 logger.info("initilizing counter for counting the words present in file.");
		int counter = 0;

		logger.info("checking whether the text file is empty or not.");
		// checking whether the text file is empty or not.
		while ((line = bufferedreader.readLine()) != null) {
			// String string = "";
			// string += line + " ";
			String string = line;
			// Converting String into String Tokenizer
			StringTokenizer st = new StringTokenizer(string);

			// checking and counting every words in file
			logger.info("Counting the words present in file");
			if (st.hasMoreTokens()) {
				counter += st.countTokens();
			}

		}
         logger.exists("Exiting the while loop.");
     
		

		System.out.println("Total words in file are: " + counter);
	}

}
