package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import org.apache.log4j.Logger;

/**
* The following program reads input text file from system, display the occurance of repeated words 
* and again write the output in seperate text file.
* @author  Pavan Patil
* @version 1.0
* @since 6-july-2017
*/

public class CountingRepeatedWords {

	
	final static Logger logger = Logger.getLogger(CountingWords.class);
		public static void main(String args[]) throws Exception
		{   
			
			Scanner sc=new Scanner(System.in);
			String outputstring="";
			logger.info("Taking input text file");
			//taking input file from system
			FileReader fr=new FileReader(new File("C:\\Users\\SRKayCG\\Desktop\\txtfile.txt"));
			FileInputStream fileInputStream = new FileInputStream("C:\\Users\\SRKayCG\\Desktop\\txtfile.txt");
			
			 //reading file
			logger.info("Reading file");
			 BufferedReader br=new BufferedReader(new InputStreamReader(fileInputStream));
			 
			 //storing text into String
			   String line = br.readLine();
			   line = line.toLowerCase();
			   System.out.println("Displaying file:");
			   System.out.println(line);
			   
			   //creating map for counting words
			   logger.info("Creating a map");
			   Map<String,Integer> countwordmap = new HashMap<>();
			   
			   //initializing the counter to 0.
			   int count = 0;
			   
			   //checking whether the string is empty or not
	                 while (line != null) {
	                	 
	                	 //splitting the words and storing it into String[]
	                     String [] words = line.split(" ");
	                     
	                     //iterating through all words
	                     for(int i=0;i<words.length;i++){
	                    	 
	                    //checking whether map already contains the word or not
	                     if(countwordmap.containsKey(words[i]))
	                     {
	                    	 countwordmap.put(words[i], countwordmap.get(words[i])+1);
	                     }
	                     
	                     //if not it is aadding the new word in map
	                     else
	                     {
	                    	 countwordmap.put(words[i],1);
	                     }
	                     }
	                     
	                     
	                     //counting the words in first line
	                     count+=words.length;
	                     line = br.readLine();
	                     if(line != null){
	                     line = line.toLowerCase();
	                     System.out.println(line);
	                     }
	                   
	                  }        
	                 //printitng total number of words
			System.out.println("\nTotal no of words: "+count);
			System.out.println("\nOccurence:\n");
			File file = new File("txtfile.txt");
			FileOutputStream fileOutputStream = new FileOutputStream(file,false);
			BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(fileOutputStream));
			System.out.println("File  :"+file.getPath());
			System.out.println("File content :");	
			
			//  printing the occurance 
			for (Entry<String, Integer> entry : countwordmap.entrySet())
		      {
				 String s = entry.getKey() + "  : "+ entry.getValue()+"\n";
				 byte b[] = s.getBytes();
				  fileOutputStream.write(b);
		          System.out.println(entry.getKey() + "  : "+ entry.getValue());
		          outputstring=outputstring+(entry.getKey() + "  : "+ entry.getValue())+ " ";
		      }
			FileWriter writer=new FileWriter(new File("C:\\Users\\SRKayCG\\Desktop\\output.txt"));
			BufferedWriter bufferedwriter=new BufferedWriter(writer);
			bufferedwriter.write(outputstring);
			bufferedwriter.flush();
			bufferedwriter.write(countwordmap.toString());
		}
	}


