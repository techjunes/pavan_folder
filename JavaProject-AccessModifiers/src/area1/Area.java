package area1;

abstract  class Area {

	public abstract double circleArea(double radius);
	public abstract double squareArea(double length,double breadth);

}
